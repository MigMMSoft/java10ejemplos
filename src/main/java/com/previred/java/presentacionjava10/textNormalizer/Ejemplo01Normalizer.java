package com.previred.java.presentacionjava10.textNormalizer;

import java.text.Normalizer;

/**
 *
 * @author mig_s
 */
public class Ejemplo01Normalizer {
    /**
     * se añade alguna nueva extensión de Unicode.
     * @param args 
     */
    public static void main(String args[]) {
        final String input = "Ejemplo: á ñ ü â è";
        System.out.println(
                Normalizer
                        .normalize(input, Normalizer.Form.NFD)
                        .replaceAll("[^\\p{ASCII}]", "")
        );
    }
}
