package com.previred.java.presentacionjava10.mejoraCopyOf;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo03MapCopyOf {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo03MapCopyOf.class.getName());

    public static void main(String args[]) {
        Map<String, String> valores = new HashMap();
        valores.put("1","2");
        valores.put("2","3");
        valores.put("3","4");
        valores.put("4","5");
        valores.put("5","6");
        valores.forEach((k, v)->LOGGER.log(Level.INFO, "Key: {0} Valor: {1}", new Object[]{k, v}));
        
        Map<String, String> valores2 = Map.copyOf(valores);
        
        //Lista copiada inmutable
        //valores2.put("Error", "Error");
    }
}
