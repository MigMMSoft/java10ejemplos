package com.previred.java.presentacionjava10.mejoraCopyOf;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo01SetCopyOf {

    private static final Logger LOGGER = Logger.getLogger(Ejemplo01SetCopyOf.class.getName());

    public static void main(String args[]) {
        Set<String> valores = new HashSet();
        valores.add("1");
        valores.add("2");
        valores.add("3");
        valores.add("4");
        valores.add("5");
        valores.stream().forEach(LOGGER::info);
        
        Set<String> valores2 = Set.copyOf(valores);
        
        //Lista copiada inmutable
        //valores2.add("Error");
    }
}
