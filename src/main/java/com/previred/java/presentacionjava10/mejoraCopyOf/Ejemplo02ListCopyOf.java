package com.previred.java.presentacionjava10.mejoraCopyOf;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo02ListCopyOf {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo02ListCopyOf.class.getName());

    public static void main(String args[]) {
        List<String> valores = new ArrayList();
        valores.add("1");
        valores.add("2");
        valores.add("3");
        valores.add("4");
        valores.add("5");
        valores.stream().forEach(LOGGER::info);
        
        List<String> valores2 = List.copyOf(valores);
        
        //Lista copiada inmutable
        //valores2.add("Error");
    }
}
