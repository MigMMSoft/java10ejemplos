package com.previred.java.presentacionjava10.mejoraOptional;

import java.util.Optional;
import java.util.logging.Logger;
import java.util.logging.Level;

/**
 *
 * @author mig_s
 */
public class Ejemplo01OrElseThrow {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo01OrElseThrow.class.getName());
    
    public static void main(String args[]) {
        Ejemplo01OrElseThrow ejemplo1 = new Ejemplo01OrElseThrow();
        
        ejemplo1.getMensaje("Optional improvement in Java 10");// Returns OK
        
//        ejemplo1.getMensaje(null);// Throws NoSuchElementException
    }
    
    public String getMensaje(String mensaje){
        final Optional<String> optional = Optional.ofNullable(mensaje);
        final String string = optional.orElseThrow();
        LOGGER.log(Level.INFO, "Mensaje: {0}", string);
        return string;
    }
}
