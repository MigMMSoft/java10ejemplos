package com.previred.java.presentacionjava10.mejoraCollectors;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author mig_s
 */
public class Ejemplo02ToUnmodifiableList {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo02ToUnmodifiableList.class.getName());
    
    public static void main(String args[]) {
        var ejemplo1 = new Ejemplo02ToUnmodifiableList();
        var lista = ejemplo1.getListaInmutable();
        LOGGER.info("Lista inmutable");
        lista.stream().forEach(System.out::println);
        
        //No se puede agregar un elemento nuevo
        //lista.add(6);
    }
    
    public List getListaInmutable(){
        var listaStream = Stream.of(1, 2, 3, 4, 5);
        return listaStream.collect(Collectors.toUnmodifiableList());
    }
}
