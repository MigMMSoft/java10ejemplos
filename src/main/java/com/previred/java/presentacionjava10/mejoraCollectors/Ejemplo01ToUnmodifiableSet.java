package com.previred.java.presentacionjava10.mejoraCollectors;

import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author mig_s
 */
public class Ejemplo01ToUnmodifiableSet {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo01ToUnmodifiableSet.class.getName());
    
    public static void main(String args[]) {
        var ejemplo1 = new Ejemplo01ToUnmodifiableSet();
        var lista = ejemplo1.getListaInmutable();
        LOGGER.info("Lista inmutable");
        lista.stream().forEach(System.out::println);
        
        //No se puede agregar un elemento nuevo
        //lista.add(6);
    }
    
    public Set getListaInmutable(){
        var listaStream = Stream.of(1, 2, 3, 4, 5);
        return listaStream.collect(Collectors.toUnmodifiableSet());
    }
}
