package com.previred.java.presentacionjava10.mejoraCollectors;

import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 * @author mig_s
 */
public class Ejemplo03ToUnmodifiableMap {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo03ToUnmodifiableMap.class.getName());
    
    public static void main(String args[]) {
        var ejemplo1 = new Ejemplo03ToUnmodifiableMap();
        var mapa = ejemplo1.getMapInmutable();
        LOGGER.info("Lista inmutable");
        mapa.forEach((a,b) -> System.out.println("K: " + a + " B: " + b));
        
        //No se puede agregar un elemento nuevo
        //mapa.put(6,2);
    }
    
    public Map getMapInmutable(){
        var listaStream = Stream.of(1, 2, 3, 4, 5);
        return listaStream.collect(Collectors.toUnmodifiableMap(Function.identity(), Function.identity()));
    }
}
