package com.previred.java.presentacionjava10.inferencia;

import com.previred.java.presentacionjava10.inferencia.varTest.TestBar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mig_s
 */
public class Ejemplo01Var {
    private static final Logger LOGGER = Logger.getLogger(Ejemplo01Var.class.getName());
    
    public static void main(String args[]) {
        var a = "";
        //no validos
//        var b;
//        var c = null;
//        var d , e = 0;
        var test = TestBar.getTest();
        LOGGER.log(Level.INFO, "Clase de tipo: {0}", test.getClass().getName());
    }
    
    //novalido
//    public void getTest1(var a){
//        //algo
//    }
}
