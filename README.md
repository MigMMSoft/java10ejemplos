### Ejemplo Java 10

Proyecto de ejemplo de nuevas funcionalidades en java 10
Estos ejemplos son para la presentación que se encuentra en:
[Prezi java 9-10-11](https://prezi.com/view/RT84iTZK97WgbScY9b5R)

## Detalle de los sistemas

Java 10
Maven 3

## Detalle de Directorios

Los ejemplos contenidos están organizados de la siguiente forma:
Ejemplos de nuevas funcionalidades con Java 10

**java10ejemplos**
Ejemplos varios de nuevas implementaciones java 10 (\src\main\java\com\previred\java\presentacionjava10)
	* inferencia
		* Ejemplo01Var.java - Ejemplo de uso de **var**
	* mejoraCollectors
		* Ejemplo01ToUnmodifiableSet.java - Ejemplo de creación de **set** inmutable
		* Ejemplo02ToUnmodifiableList.java - Ejemplo de creación de **list** inmutable
		* Ejemplo03ToUnmodifiableMap.java - Ejemplo de creación de **map** inmutable
	* mejoraCopyOf
		* Ejemplo01SetCopyOf.java - Ejemplo de creación de **set** inmutable
		* Ejemplo02ListCopyOf.java - Ejemplo de creación de **lista** inmutable
		* Ejemplo03MapCopyOf.java - Ejemplo de creación de **map** inmutable
	* mejoraOptional
		* Ejemplo01OrElseThrow.java - Ejemplo de nueva opción **OrElseThrow** de **optional**
	* textNormalizer
		* Ejemplo01Normalizer.java - Ejemplo básico de normalización de **string**